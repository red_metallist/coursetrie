/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursetrie;

/**
 *
 * @author Николай
 */
class TrieNode
{
    char letter;
    TrieNode[] links;
    boolean fullWord;
    int frequency;
    
    TrieNode(char letter)
    {
        this.letter = letter;
        links = new TrieNode[26]; //26
        this.frequency=0;
        this.fullWord = false;
    }
}
