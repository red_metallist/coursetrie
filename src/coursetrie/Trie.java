/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursetrie;

/**
 *
 * @author Николай
 */
public class Trie {
    
     public  void insertWord(TrieNode root, String word)
    {
        int offset = 97; //97     97-122
        int l = word.length();
        char[] letters = word.toCharArray();
        TrieNode curNode = root;
        
        for (int i = 0; i < l; i++)
        {
            if (curNode.links[letters[i]-offset] == null)
                curNode.links[letters[i]-offset] = new TrieNode(letters[i]);
            curNode = curNode.links[letters[i]-offset];
        }
        curNode.frequency+=1;
        curNode.fullWord = true;  
    }
     public  void insertFromDictionary(TrieNode root, String word,int frequency)
    {
        int offset = 97; //97     97-122
        int l = word.length();
        char[] letters = word.toCharArray();
        TrieNode curNode = root;
        
        for (int i = 0; i < l; i++)
        {
            if (curNode.links[letters[i]-offset] == null)
                curNode.links[letters[i]-offset] = new TrieNode(letters[i]);
            curNode = curNode.links[letters[i]-offset];
        }
        curNode.frequency=frequency;
        curNode.fullWord = true;  
    }

    public void navigate(TrieNode root, String word,QueueNode [] queue)
    {
        char[] branch = new char[50];
        
        char[] letters = word.toCharArray();
      for(int a=0;a<letters.length;a++)
      { branch[a]=letters[a];}
      
      
        int l = letters.length;
        int offset = 97;
        TrieNode curNode = root;
        
        int i;
        boolean flag=true;
        for (i = 0; i < l; i++)
        {
            if(curNode.links[letters[i]-offset]!=null)
                
            curNode = curNode.links[letters[i]-offset];
            else
            {flag=false;
            break;}
        }
        if(flag==true)
        RecMoving(curNode,l-1,branch,queue);
    }
    
    static void RecMoving(TrieNode root, int level, char[] branch,QueueNode [] queue)
    {
        if (root == null)
            return;
        
        for (int i = 0; i < root.links.length; i++) //ходим по буквам алфавита
        {
            branch[level] = root.letter;
            RecMoving(root.links[i], level+1, branch,queue);    
        }
        
        if (root.fullWord)
        {
            String str="";
            for (int j = 0; j <= level; j++)
            {str+=branch[j];
            }
            Queue.AddQueue(queue,str,root.frequency);
        }
    }
    
     static void printTree(TrieNode root, int level, char[] branch) //выводит все дерево целиком. осталось с первых версий.
    {
        if (root == null)
            return;
        
        for (int i = 0; i < root.links.length; i++) //ходим по буквам алфавита
        {
            branch[level] = root.letter;
            printTree(root.links[i], level+1, branch);    
        }
        
        if (root.fullWord)
        {
            for (int j = 1; j <= level; j++)
                System.out.print(branch[j]);
            System.out.println();
            System.out.println(root.frequency);
        }
    }
     
      static TrieNode createTree()
    {
        return(new TrieNode('\0'));
    }
    
}
