/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursetrie;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Николай
 */
public class ReadFromBook implements IReader {

    public void reader(String filename, TrieNode tree, Trie trie) 
    {
        Scanner scaner = null;
        try {
            scaner = new Scanner(new File(filename), "utf-8");     //открываем файл
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadFromBook.class.getName()).log(Level.SEVERE, null, ex);
        }

        while (scaner.hasNext()) //пока есть слова в файле
        {

            String word = scaner.next();  //берем очередное слово

            trie.insertWord(tree, word);
        }
        scaner.close();
    }

}
