/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coursetrie;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Николай
 */
public class ReadFromDictionary implements IReader {

    public void reader(String filename,TrieNode tree,Trie trie)     //метод для чтения слов из указанного файла
    {
        Scanner in = null;
        try {
            in = new Scanner(new File(filename), "utf-8");     //открываем файл
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadFromDictionary.class.getName()).log(Level.SEVERE, null, ex);
        }
        while (in.hasNext())    //пока есть слова в файле
        {  
            String word=in.next();  //берем очередное слово
            String [] mass=word.split("\\|");
           
             trie.insertFromDictionary(tree, mass[0],Integer.valueOf(mass[1]));
        }
        
        in.close();    
}
    
}
